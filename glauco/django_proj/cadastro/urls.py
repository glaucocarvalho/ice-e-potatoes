from django.contrib import admin
from django.urls import path, re_path
from cadastro import views

urlpatterns = [
    path('', views.index),
    path('post/', views.testPost),
    path('api/getUsers/', views.getUsers),
    re_path(r'^report/(?P<name>[\w ]+)/(?P<encoded>[\w \- \, \. \[ \] ]+)/$', views.report),
    re_path(r'^report/(?P<encoded>[\w \- \, \. \[ \] ]+)/$', views.report),
]