from django.contrib import admin
from .models import *


class UsuarioAdmin(admin.ModelAdmin):
    fields = ('name', 'image', 'email')

admin.site.register(Usuario, UsuarioAdmin)


class AcessosAdmin(admin.ModelAdmin):
    fields = ('name', 'date', 'status')
    list_display = ('name', 'date', 'Reconhecido')
    def Reconhecido(self, obj):
        return obj.status
    Reconhecido.boolean = True

admin.site.register(Acessos, AcessosAdmin)
