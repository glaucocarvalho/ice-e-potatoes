from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, request
from urllib import request as urlRequest
from urllib import parse
import json, pprint, requests, face_recognition
from .models import *


TEST_FACE = '[-0.06718673557043076, 0.12670348584651947, 0.043037861585617065, -0.08189278841018677, ' \
            '-0.007036920171231031, -0.04039837047457695, -0.054573845118284225, -0.09895976632833481, ' \
            '0.1876624971628189, -0.0532410554587841, 0.2690076231956482, 0.036727286875247955, -0.17661282420158386, ' \
            '-0.19215938448905945, -0.012859273701906204, 0.10408266633749008, -0.05011880397796631, ' \
            '-0.1385166347026825, -0.04107724130153656, -0.06827376782894135, 0.08272199332714081, ' \
            '0.06399529427289963, 0.018658040091395378, 0.054731905460357666, -0.1670592576265335, ' \
            '-0.3453826606273651, -0.11666736751794815, -0.06734628975391388, -0.021729007363319397, ' \
            '-0.09320270270109177, 0.054476048797369, 0.18332089483737946, -0.13896682858467102, ' \
            '-0.02678362838923931, 0.058417029678821564, 0.12609852850437164, 0.007966931909322739, ' \
            '0.018726691603660583, 0.1489473432302475, 0.08486572653055191, -0.13460658490657806, ' \
            '0.03468046337366104, 0.024972762912511826, 0.3369379937648773, 0.1675426959991455, 0.009657930582761765, ' \
            '0.04520922899246216, -0.015449928119778633, 0.06674699485301971, -0.16604335606098175, ' \
            '0.08489401638507843, 0.1494903266429901, 0.12626630067825317, 0.04673588275909424, 0.15795885026454926, ' \
            '-0.11961929500102997, -0.004350453615188599, 0.11906160414218903, -0.22287899255752563, ' \
            '0.04086284339427948, 0.0018369285389780998, -0.1191156730055809, -0.09154269844293594, ' \
            '-0.09860636293888092, 0.18935096263885498, 0.13043251633644104, -0.1217079684138298, ' \
            '-0.11156180500984192, 0.17738665640354156, -0.07176033407449722, -0.05238441377878189, ' \
            '0.09675708413124084, -0.16935686767101288, -0.14473901689052582, -0.22214488685131073, ' \
            '0.14162084460258484, 0.3732336163520813, 0.16189157962799072, -0.21758891642093658, 0.05454521253705025, ' \
            '-0.02317952550947666, 0.01418638788163662, 0.01694413647055626, -0.0015412869397550821, ' \
            '-0.08303340524435043, -0.008541624993085861, -0.06126321107149124, 0.019021395593881607, ' \
            '0.12727493047714233, 0.05902395397424698, -0.07472048699855804, 0.20487117767333984, ' \
            '-0.05438663065433502, 0.03448556363582611, 0.04308167099952698, -0.0530039481818676, ' \
            '-0.06075106933712959, 0.007986368611454964, -0.1435941457748413, -0.051324550062417984, ' \
            '0.007295642048120499, -0.1222057193517685, -0.006588621996343136, 0.06925167888402939, ' \
            '-0.1489846557378769, 0.12658622860908508, 0.037760455161333084, -0.015494579449295998, ' \
            '-0.02268117293715477, 0.10496789216995239, -0.13942210376262665, -0.022718574851751328, ' \
            '0.12636864185333252, -0.2873840034008026, 0.25678351521492004, 0.11195565015077591, 0.06461898237466812, ' \
            '0.15305374562740326, 0.09251734614372253, 0.09876936674118042, -0.000795481726527214, ' \
            '-0.0743342787027359, -0.04640975967049599, -0.09213633090257645, 0.09685157984495163, ' \
            '-0.021878091618418694, 0.08033029735088348, -0.006921649910509586] '


def index(request):
    return HttpResponse('index de cadastro')


def getUsers(request):
    data = {}
    for user in Usuario.objects.all():
        if user.id not in data:
            data[user.id] = {}
        data[user.id]['name'] = user.name
        data[user.id]['email'] = user.email
        data[user.id]['encoded_image'] = user.encoded_image
    return JsonResponse(data)


def report(request, name='', encoded=''):
    ret = ''
    if name != '':  # processamento no cliente(raspberry)
        ret = 'Rosto reconhecido. Salvando acesso.'
        Acessos(name=name, status=1, encoded_image=encoded).save()
    if name == '':  # processamento no servidor
        # data uma imagem devo processá-la para procurar um rosto conhecido
        print("TODO: processamento no servidor")

    return HttpResponse(ret)


def testPost(request):
    """
    rest problems, estudar https://www.django-rest-framework.org/topics/api-clients/#getting-started
    :param request:
    :return:
    """
    web_service_ip_port = 'http://127.0.0.1:8000/'
    api_path = 'cadastro/api/report/'
    url = str(web_service_ip_port)+str(api_path)
    client = requests.session()

    # Retrieve the CSRF token first
    client.get(web_service_ip_port)  # sets cookie
    pprint.pprint(client.cookies)
    # try:
    #     csrftoken = client.cookies['csrftoken']
    # except KeyError:
    #     csrftoken = client.cookies['c/srf']
    # pprint.pprint(csrftoken)
    data = {
        # 'csrfmiddlewaretoken': csrftoken,
        'name': 'teste de post',
        'id': 12,
        'list': [1, 2, 3],
    }

    # json_data = json.dumps(data).encode('utf-8')
    json_data = parse.urlencode(data).encode('utf-8')
    url_request = urlRequest.Request(
                                    url,
                                    # data=json_data
                                    )
    resp = urlRequest.urlopen(url_request).read()
    return HttpResponse(resp.decode('utf-8'))


    # coreapi
    # import coreapi
    # client = coreapi.Client()
    # schema = client.get(url)
    #
    # action = ['api-token-auth', 'create']
    # params = {"username": "example", "password": "secret"}
    # result = client.action(schema, action, params)
    #
    # auth = coreapi.auth.TokenAuthentication(
    #     scheme='JWT',
    #     token=result['token']
    # )
    # client = coreapi.Client(auth=auth)
    # return HttpResponse('test')
