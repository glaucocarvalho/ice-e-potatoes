from django.db import models
from django.utils.timezone import now
import face_recognition
import json

class Usuario(models.Model):
    name = models.CharField(max_length=200, default='')
    image = models.ImageField(null=True, default=None)
    encoded_image = models.TextField(default='', blank=True)
    email = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.name

    def save(self):
        if self.image:
            dummie = face_recognition.load_image_file(self.image)
            try:
                self.encoded_image = json.dumps(face_recognition.face_encodings(dummie)[0].tolist())
            except IndexError:
                self.encoded_image = json.dumps(face_recognition.face_encodings(dummie))
        super(Usuario, self).save()


class Acessos(models.Model):
    status_choices = {
        ('0', 'Não reconhecido'),
        ('1', 'Reconhecido')
    }
    name = models.CharField(max_length=200, default='')
    date = models.DateTimeField(default=now)
    status = models.IntegerField(default=0, choices=status_choices)
    encoded_image = models.TextField(default='', blank=True)
    image = models.ImageField(null=True, default=None)
