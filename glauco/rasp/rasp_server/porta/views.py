from django.http import HttpResponse
import time
try:
    import RPi.GPIO as gpio
    import gpiozero
    RASPBERRY = True
except RuntimeError:
    print('Nao estou num raspberry, executando modo notebook')
    RASPBERRY = False


def trancar(request):
    if RASPBERRY:
        lock_pin = 21
        TIMER = 10
        now = time.time()

        led = gpiozero.LED(lock_pin)
        led.off()
    return HttpResponse('trancar porta')


def destrancar(request):
    if RASPBERRY:
        lock_pin = 21
        TIMER = 10
        now = time.time()

        led = gpiozero.LED(lock_pin)
        while time.time() - now < TIMER:
            led.on()
    return HttpResponse('destrancar porta')
