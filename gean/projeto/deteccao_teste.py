import cv2
import dlib
 
fonte = cv2.FONT_HERSHEY_COMPLEX_SMALL
webcam = cv2.VideoCapture(0)
detectorFace = dlib.get_frontal_face_detector()
detectorPontosFaciais = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
 
conectado, imagem = webcam.read()
while (True):
    conectado, imagem = webcam.read()
    facesDetectadas = detectorFace(imagem, 0) #Caixa de Boulding Box
 
    for face in facesDetectadas:
        pontos = detectorPontosFaciais(imagem, face)
        print("Face Encontrada!")
        print(pontos.parts())
        print("Número de pontos:")
        print(len(pontos.parts()))
        print("===================================")
    cv2.imshow('Pontos Faciais', imagem)
    if cv2.waitKey(1) & 0xFF == 27:
        break

webcam.release()
cv2.destroyAllWindows()

