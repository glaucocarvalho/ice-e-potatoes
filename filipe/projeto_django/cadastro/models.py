from django.db import models
import face_recognition
import json
from .helper import validate_CPF, DV_maker
from django.core.validators import validate_email

from fractions import Fraction
from camera_imagefield import CameraImageField
from django import forms

class Usuario(models.Model):
    name = models.CharField(max_length=200, default='')
    image = models.ImageField(null=True, default=None)
    #image2 = forms.CameraImageField(null=True, default=None)
    encoded_image = models.TextField(default='', blank=True)
    email = models.CharField(max_length=200, default='')
    data = models.DateTimeField('Data de cadastramento')
    cpf = models.CharField('CPF',unique=True, max_length=14, validators=[validate_CPF])

class MyForm(forms.Form):
    landscape = CameraImageField(aspect_ratio=Fraction(16, 9))

    def __str__(self):
        return self.name

    def save(self):
        if self.image:
            dummie = face_recognition.load_image_file(self.image)
            self.encoded_image = json.dumps(face_recognition.face_encodings(dummie)[0].tolist())
        super(Usuario, self).save()
