from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, request
from urllib import request as urlRequest
from urllib import parse
import json, pprint, requests
from .models import *


def index(request):
    return HttpResponse('index de cadastro')


def getUsers(request):
    data = {}
    for user in Usuario.objects.all():
        if user.id not in data:
            data[user.id] = {}
        data[user.id]['name'] = user.name
        data[user.id]['email'] = user.email
        data[user.id]['encoded_image'] = user.encoded_image
    return JsonResponse(data)


def report(request):
    # pprint.pprint(request.POST().copy())
    return HttpResponse('report view response')


def testPost(request):
    """
    rest problems, estudar https://www.django-rest-framework.org/topics/api-clients/#getting-started
    :param request:
    :return:
    """
    web_service_ip_port = 'http://127.0.0.1:8000/'
    api_path = 'cadastro/api/report/'
    url = str(web_service_ip_port)+str(api_path)
    client = requests.session()

    # Retrieve the CSRF token first
    client.get(web_service_ip_port)  # sets cookie
    pprint.pprint(client.cookies)
    # try:
    #     csrftoken = client.cookies['csrftoken']
    # except KeyError:
    #     csrftoken = client.cookies['c/srf']
    # pprint.pprint(csrftoken)
    data = {
        # 'csrfmiddlewaretoken': csrftoken,
        'name': 'teste de post',
        'id': 12,
        'list': [1, 2, 3],
    }

    # json_data = json.dumps(data).encode('utf-8')
    json_data = parse.urlencode(data).encode('utf-8')
    url_request = urlRequest.Request(
                                    url,
                                    # data=json_data
                                    )
    resp = urlRequest.urlopen(url_request).read()
    return HttpResponse(resp.decode('utf-8'))


    # coreapi
    # import coreapi
    # client = coreapi.Client()
    # schema = client.get(url)
    #
    # action = ['api-token-auth', 'create']
    # params = {"username": "example", "password": "secret"}
    # result = client.action(schema, action, params)
    #
    # auth = coreapi.auth.TokenAuthentication(
    #     scheme='JWT',
    #     token=result['token']
    # )
    # client = coreapi.Client(auth=auth)
    # return HttpResponse('test')
