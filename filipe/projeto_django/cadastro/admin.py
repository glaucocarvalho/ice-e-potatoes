from django.contrib import admin
from .models import *


# Register your models here.
class UsuarioAdmin(admin.ModelAdmin):
    fields = ('name', 'email', 'cpf', 'data', 'image')

admin.site.register(Usuario, UsuarioAdmin)
# admin.site.register(Usuario)
