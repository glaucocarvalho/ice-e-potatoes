from django.contrib import admin
from django.urls import path
from cadastro import views

urlpatterns = [
    path('', views.index),
    path('post/', views.testPost),
    path('api/getUsers/', views.getUsers),
    path('api/report/', views.report),
    #url(r'^photo/', include('photo_upload.urls'))
]
