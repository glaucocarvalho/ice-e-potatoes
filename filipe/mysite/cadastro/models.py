from django.db import models
from .helper import validate_CPF, DV_maker
from django.core.validators import validate_email

# Create your models here.

class Usuario(models.Model):
    user_id = models.CharField('Nome',max_length=200)
    user_date = models.DateTimeField('Data de cadastramento')
    user_cpf = models.CharField('CPF',unique=True, max_length=14, validators=[validate_CPF])
    user_email = models.CharField('E-mail',unique=True, max_length=100, validators=[validate_email])
    user_image = models.ImageField('Foto', upload_to="gallery")

    def __str__(self):
        return self.user_id

class Userhistory(models.Model):
    username_history = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    userhistory_text = models.CharField(max_length=200)
    contador = models.IntegerField(default=0)

    def __str__(self):
        return self.userhistory_text
