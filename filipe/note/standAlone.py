import face_recognition, cv2, os, json, time
from urllib import request, parse
import pprint
try:
    import RPi.GPIO as gpio
    RASPBERRY = True
except RuntimeError:
    print('Nao estou num raspberry, executando modo notebook')
    RASPBERRY = False

"""
TODO:
* durante o sleep, a aplicacao inteira para, seria interessante fazer o sleep num thread separado
e um aviso na tela do tipo "porta destrancada por %timer%" onde timer e o tempo regressivo.
"""

########################
###    constaints    ###
########################
RUN = True                      # If you need you can turn the main loop off.
lock_pin = 3	                # Logical pin of Raspberry.
TIMER_OPEN = 10                 # Timer in secconds to keep the door unlocked.
TIMER_UNLOCK = 10               # Timer in secconds to keep a face on screen to unlock the door.
TIMER_UPDATE = 10               # Timer to update the list in case of a unknown face.
TIMER_CONSOLE = 30              # Timer to write same message on the console.
TIMER_REPORT = TIMER_UNLOCK     # Timer to report a unknown face trying to enter
# web_service_ip_port = '192.168.0.165:8080/'
web_service_ip_port = '127.0.0.1:8000/'
########################
### useful functions ###
########################


def send_report(data):
    """
    data = {
        'type' : acesso|tentativa de acesso,
        'name' : name|unknown,
        'encoded_face' : json.dumps(encoded_face),
    }
    :param data:
    :return:
    """
    api_path = 'cadastro/api/report/'

    json_data = json.dumps(data)
    url_request = request.Request('http://'+str(web_service_ip_port)+str(api_path), data=json_data)



def get_faces_from_server():
    # TODO: atualizar configurações tbm
    api_path = 'cadastro/api/getUsers/'

    known_face_encodings = []
    known_face_names = []

    url_request = request.Request('http://'+str(web_service_ip_port)+str(api_path))
    response = json.loads(request.urlopen(url_request).read().decode('utf-8'))
    for key, person in response.items():
        known_face_encodings.append(json.loads(person['encoded_image']))
        known_face_names.append(person['name'])
    return known_face_encodings, known_face_names


def get_faces_from_images():
    images_path = 'imagens/'
    images = []
    for root, dirs, files in os.walk(images_path):
        for filename in files:
            if '.jpg' in filename:
                images.append(filename)

    known_face_encodings = []
    known_face_names = []
    for image in images:
        person = face_recognition.load_image_file(str(images_path) + str(image))
        encoded = face_recognition.face_encodings(person)[0]
        known_face_encodings.append(encoded)
        known_face_names.append(str(image)[:-4])
    return known_face_encodings, known_face_names


def door(status):
    """
    GPIO control of Raspberry, reference: https://www.elektronik-kompendium.de/sites/raspberry-pi/1907101.htm
    :param status:
    :return:
    """
    if status == 'open':
        print('Ativar rele')
        if RASPBERRY:
            gpio.output(lock_pin, gpio.HIGH)
    elif status == 'close':
        print('Desativar rele')
        if RASPBERRY:
            gpio.output(lock_pin, gpio.LOW)


#########################
### main code routine ###
#########################

# Get a reference to webcam #0 (the default one)
video_capture = cv2.VideoCapture('127.0.0.1:5000/') #colocar o ip da camera ip entre ()

# Get faces
# known_face_encodings, known_face_names = get_faces_from_images()
try:
    known_face_encodings, known_face_names = get_faces_from_server()
except:
    print('Is the server online? maybe in a diferent address?')
    RUN = False

# Initialize some variables
process_this_frame = True
face_locations = []
face_encodings = []
face_names = []
timer_unlock = 0
timer_update = 0
timer_console = 0
timer_report = 0

if RASPBERRY:
    # configure gpio as BOARD
    gpio.setmode(gpio.BOARD)
    # configure pin as output
    gpio.setup(lock_pin, gpio.OUT)

while RUN:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    report_face = ''
    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        # for face_encoding in face_encodings:  # see a face at a time ?
        # See if the face is a match for the known face(s)
        # matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        try:
            report_face = face_encodings[0]
            matches = face_recognition.compare_faces(known_face_encodings, report_face)
        except:
            if timer_console == 0:
                timer_console = time.time()
            elif time.time() - timer_console >= TIMER_CONSOLE:
                timer_console = 0
                print('Nenhuma face encontrada')
            matches = []

        name = "None"
        # If a match was found in known_face_encodings, just use the first one.
        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_names[first_match_index]
            if timer_unlock == 0:  # start timer
                timer_unlock = time.time()
            elif time.time() - timer_unlock >= TIMER_UNLOCK:  # se passou 15 sec, ativar rele
                timer_unlock = 0
                # TODO: enviar relatório de acesso | TEST ME!
                data = {
                    'type': 'acesso',
                    'name': name,
                    'encoded_face': report_face.tolist()
                }
                send_report(data)
                door('open')
                time.sleep(TIMER_OPEN)
                door('close')
        elif True not in matches:
            timer_unlock = 0
            name = "Unknown"
        face_names.append(name)
    process_this_frame = not process_this_frame


    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # TODO: TEST ME!
        # If is a unknown face
        if name == "Unknown":
            # reset timer_unlock
            if timer_report == 0:
                timer_report = time.time()
            elif time.time() - timer_report >= TIMER_REPORT:
                timer_report = 0
                print('Tentativa de acesso negada, enviando relatório')
                # TODO: enviar relatório de tentativa de acesso | TEST ME!
                data = {
                    'type': 'tentativa de acesso',
                    'name': name,
                    'encoded_face': report_face.tolist()
                }
                send_report(data)

            if timer_update == 0:
                timer_update = time.time()
            elif time.time() - timer_update >= TIMER_UPDATE:
                timer_update = 0
                print('Rosto desconhecido. Atualizando faces conhecidas...')
                try:
                    known_face_encodings, known_face_names = get_faces_from_server()
                except:
                    print('something went wrong, maybe the server?')

        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
if RASPBERRY:
    # Clean GPIO
    gpio.cleanup()
cv2.destroyAllWindows()
