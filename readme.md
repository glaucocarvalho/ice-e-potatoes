# ice-e-potatoes

A college class project using face recognition with python.

## getting started

pip3 install face_recognition opencv-python

apt install cmake

apt  install libqtgui4

## exemplo de reconhecimento facial na webcam

cd glauco

python3 facerec_from_webcam_faster.py

tecla "q" para sair


## Como colaborar

* Em sua máquina crie um virtualenv com os parâmetros -python3 --always-copy
* entre no diretório deste virtualenv
* git clone https://gitlab.com/glaucocarvalho/ice-e-potatoes
* Navegue para sua respectiva pasta de colaborador e trabalhe nela

# Não altere o trabalho dos amiguinhos sem o consentimento prévio!

* faça commits separados de acordo com a responsabilidade de cada tarefa
* Ao final de seu trabalho git push origin --all e use suas credenciais conforme forem pedidas
** (duvidas) veja "Adicionar este remote"

## Adicionar este remote

git remote add origin https://gitlab.com/glaucocarvalho/ice-e-potatoes.git

### links úteis
Escrevendo arquivos readme.md: 
https://gist.github.com/PurpleBooth/109311bb0361f32d87a2

Escrevendo bons commits: 
https://medium.com/@rafael.oliveira/como-escrever-boas-mensagens-de-commit-9f8fe852155a

Documentação face_recognition: 
https://face-recognition.readthedocs.io/en/latest/

Normalização de código: 
http://pep8online.com/

How to git:
http://rogerdudler.github.io/git-guide/

Django:
https://www.djangoproject.com/start/

virtualenv:
https://virtualenv.readthedocs.io/en/latest/userguide/

pip3:
https://pip-python3.readthedocs.io/en/latest/
